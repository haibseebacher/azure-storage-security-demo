﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Services.AppAuthentication;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Auth;
using Microsoft.Azure.Storage.Blob;

namespace filedelivery_aad
{
    class Program
    {
        static void Main(string[] args)
        {
            var provider = new AzureServiceTokenProvider("RunAs=Developer;DeveloperTool=AzureCLI");
            try
            {
                var token =  provider.GetAccessTokenAsync("https://istatdataservice.blob.core.windows.net/").Result;

                var tokenCredential = new TokenCredential(token);
                var storageCredentials = new StorageCredentials(tokenCredential);

                var container = new CloudBlobContainer(
                    new Uri("https://istatdataservice.blob.core.windows.net/publicationstage"), 
                    storageCredentials);

                container.CreateIfNotExists();
                var blobRef = container.GetBlockBlobReference("test.txt");
                blobRef.UploadFromFileAsync("/Users/brianseebacher/Documents/source/prototype/filedelivery-aad/test.txt").Wait();

            }
            catch (AzureServiceTokenProviderException ex)
            {
                Console.WriteLine($"Exception from Azure Service Token Provider. Check login status. : {ex.Message}");
            }
        }
    }
}
